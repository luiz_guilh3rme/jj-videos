<?php 

/*
The template for displaying 404 pages (Not Found)
Template Name: Thanks
*/

get_header(); ?>

<!--linear-gradient(to right, #0de1ea, #c6d92c)-->
<section class="container-fluid video_destaque" id="video_destaque" name="videos">
	<div class="container">
		<div class="row">
			<div class="col-xs-24">
				<h1 class="title"><center>Erro 404</center></h1>
				A página que você procurou não está em nossos servidores.
			</div>
		</div>
	</div>  
	
	<div class="container">
		<div class="row">
			<div class="col-xs-14">
				<?php if (have_posts()) : ?>
					 <?php while (have_posts()) : the_post(); ?>
							<?php the_content(); ?>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
   </div>  
</section>
<?php get_footer(); ?>

