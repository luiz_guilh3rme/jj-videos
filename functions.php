<?php

// ========================
// ADICIONA FUNÇÕES AO TEMA
// ========================
add_action('after_setup_theme', 'custom_setup');

function custom_setup() {

    // Oculta a barra de admin no front
    add_filter('show_admin_bar', '__return_false');

    // Executa as função ao instalar o tema
    add_action('init', 'custom_init');

    // Insere a opção de imagens destacadas
    add_theme_support('post-thumbnails');
    add_image_size('home-thumbnails', 480, 350, true);
    add_image_size('post-thumbnails', 1000, 500, array('left','top'));

    // Insere a opção de menus
    add_theme_support('menus');

    /* Carrega scripts e estilos personalizados */
    add_action('wp_enqueue_scripts', 'custom_formats');
}

// =================================================
// FUNÇÃO PARA REGISTRAR E EXIBIR ARQUIVOS DE ESTILO
// =================================================

function custom_formats() {

    wp_register_style('style', PW_THEME_URL . 'css/style.css', null, null, 'all'); // (função, caminho, dependência, versão, mídia)

    wp_register_script('esc-jquery', PW_THEME_URL . 'js/jquery-1.10.1.min.js', null, null, true);
    wp_register_script('main', PW_THEME_URL . 'js/main.js', array('esc-jquery'), null, true);



    // Carrega as folhas de estilo no head
    wp_enqueue_style('style');


    // Carrega os scripts no footer
    wp_enqueue_script('esc-jquery');
    wp_enqueue_script('main');
}

function custom_init() {
    // ============================
    // CRIA O CUSTOM POST TYPE BLOG
    // ============================
    $attr = array(
        'public' => true, //Tira as propriedades de exibição do site, não terá uma url para esse tipo de post
        'show_ui' => true, //Permite que seja exibido no admin
        'supports' => array('title', 'editor', 'page-attributes', 'thumbnail'), // Exibe as informações necessárias, nesse caso: Título, Editor de Texto, Ordenação, Imagem destacada
        'labels' => array(
            'name' => 'News',
            'singular_name' => 'News'
        ),
    );
    register_post_type(CPT_BLOG, $attr);

    // Permite que o wordpress não verifique sempre as opções de reescrita de URL, com isso sobrecarga desnecessária
    // Ele somente fará a verificação se o valor for diferente da definição
    // Isso permite que eu possa alterar a url de todos os posts de um tipo, desde que eu identifique na definição da função como 'second' ou sucessivamente
    if (get_option('custom_permalinks') !== CUSTOM_PERMALINKS) {
        update_option('custom_permalinks', CUSTOM_PERMALINKS);
        flush_rewrite_rules();
    }

    register_taxonomy("blog-categories", array(CPT_BLOG), array(
        "hierarchical" => true,
        "label" => "Categories",
        "singular_label" => "Category",
        "rewrite" => array(
            'slug' => 'blog',
            'with_front' => false)
            )
    );
}

define('CPT_BLOG', 'blog');
define('CUSTOM_PERMALINKS', 2);


// ======================================================
// DEFINIÇÃO DE CAMINHOS PARA REDUZIR PROCESSAMENTO DO WP
// ======================================================
define('PW_URL', get_home_url()); /* <?php echo PW_URL; ?> */
define('PW_THEME_URL', get_bloginfo('template_url') . '/'); /* <?php echo PW_THEME_URL; ?> */
define('PW_SITE_NAME', get_bloginfo('name')); /* <?php echo PW_SITE_NAME; ?> */
define('PW_SITE_DESCRIPTION', get_bloginfo('description')); /* <?php echo PW_SITE_DESCRIPTION; ?> */


// ==================================================
// CARREGA O TEMPLATE DA CATEGORIA MÃE PARA AS FILHAS
// ==================================================
function load_cat_parent_template($template) {

    $cat_ID = absint(get_query_var('cat'));
    $category = get_category($cat_ID);

    $templates = array();

    if (!is_wp_error($category))
        $templates[] = "category-{$category->slug}.php";

    $templates[] = "category-$cat_ID.php";

    if (!is_wp_error($category)) {
        $category = $category->parent ? get_category($category->parent) : '';

        if (!empty($category)) {
            if (!is_wp_error($category))
                $templates[] = "category-{$category->slug}.php";

            $templates[] = "category-{$category->term_id}.php";
        }
    }

    $templates[] = "category.php";
    $template = locate_template($templates);

    return $template;
}

add_action('category_template', 'load_cat_parent_template');



function new_excerpt_length($length) {
	return 30;
}
add_filter('excerpt_length', 'new_excerpt_length');

// ============================================
// FAZ COM QUE A BUSCA PROCURE SOMENTE EM POSTS
// ============================================

//function SearchFilter($query) {
//    if ($query->is_search) {
//        $query->set('post_type', 'post');
//    } return $query;
//}
//add_filter('pre_get_posts', 'SearchFilter');
