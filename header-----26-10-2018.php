<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/> -->

    <meta name="apple-mobile-web-app-title" content="Homepage" />

    <meta name="logo150x150" content=".png" />

    <!--[if lte IE 8]-->

    <title>
        <?php echo PW_SITE_NAME; ?>
    </title>

    <meta content="<?php echo PW_SITE_DESCRIPTION; ?>" name="description">



    <!-- Favicon -->

    <!--

        <link rel="shortcut icon" href="icon.ico">

        <link rel="shortcut icon" href="icon.png">

        <link rel="icon" href=".png" sizes="152x152"/>

    -->



    <!-- Bootstrap -->

    <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/bootstrap/css/bootstrap.min.css">

    <!--<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>bootstrap/css/bootstrap-theme.min.css">-->

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt"
    crossorigin="anonymous">

    <!-- CSS -->

    <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">

    <!--<link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>css/animate.min.css">-->





    <!-- JS LINK -->

    <!--<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>js/modernizr.js"></script>-->



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>

        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

        <![endif]

    -->

    <?php wp_head(); ?>







    <!-- Facebook Pixel Code -->

    <script>
        ! function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?

                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;

            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;

            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,

            document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');



        fbq('init', '495518443963236');

        fbq('track', "PageView");
    </script>

    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=495518443963236&ev=PageView&noscript=1" /></noscript>

    <!-- End Facebook Pixel Code -->



    <script src="//code.tidio.co/b5kl77iqo2kz1pax5svsfzozhojc6buq.js"></script>

</head>
<header class="container-fluid header-nav">
    <div class="row">

        <div class="col-xs-24 navigation no-pad">

            <div class="col-xs-24 logo-mobile">
                <a href="http://www.jjvideos.com.br/">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.png" alt="logo jjvideos">
                </a>
                <div id="bt-mobile" class="header-toggle">
                    <i class="icon-nav">
                        <span class="inner-icon-nav"></span>
                    </i>
                </div>
            </div>

            <div class="col-xs-24 menu">
                <a class="logo" href="http://www.jjvideos.com.br" title="Ir para Home"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.png"
                    alt="logo jjvideos"></a>
                    <ul class="menu-list">
                        <li class="sub-nav"><a title="Ir para Planos">serviços</a>
                            <div class="columns-wrapper">
                                <ul>
                                    <li>
                                        <a href="<?= site_url('/videos-animados/'); ?>" title="Ir para Videos Animados">
                                        Vídeos Animados</a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('/videos-explicativos/'); ?>" title="Ir para Videos Explicativos">
                                        Vídeos Explicativos</a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('/videos-produtos-servicos/'); ?>" title="Ir para Vídeos Produtos/Serviços">
                                        Videos para Produtos / Serviços</a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('/videos-marketing/'); ?>" title="Ir para Vídeos Marketing">
                                        Vídeos Marketing</a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('/videos-para-web/'); ?>" title="Ir para Vídeos Web">
                                        Vídeos para Web</a>
                                    </li>
                                </ul>
                                <ul>
                                    <li>
                                        <a href="<?= site_url('/videos-institucionais/'); ?>" title="Ir para Vídeos Institucionais">
                                        Vídeos Institucionais</a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('/videos-infograficos/'); ?>" title="Ir para Vídeos Infográficos">
                                        Vídeos Infográficos</a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('/videos-de-apresentacao/'); ?>" title="Ir para Vídeos Apresentação">
                                        Vídeos de Apresentação</a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('/videos-de-animacao-3d/'); ?>" title="Ir para Vídeos de Animação 3D">
                                        Vídeos de Animação 3D</a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('/videos-de-animacao-2d/'); ?>" title="Ir para Vídeos de animação 2D">
                                        Vídeos de Animação 2D</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li><a href="<?= site_url('#portfolio'); ?>" title="Ir para Portfólio">portfólio</a></li>
                        <li class="sub-nav"><a title="Ir para Planos">planos</a>
                            <div class="columns-wrapper">
                                <ul>
                                    <li>
                                        <a href="<?= site_url('/pacote-basico/'); ?>" title="Ir para Pacote Básico">
                                        Básico</a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('/pacote-profissional/'); ?>" title="Ir para Pacote Profissional">
                                        Profissional</a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('/pacote-premium/'); ?>" title="Ir para Pacote Premium">
                                        Premium</a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('/pacote-expert/'); ?>" title="Ir para Pacote Expert">
                                        Expert</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li><a href="<?= site_url('#blog'); ?>" title="Ir para Blog">blog</a></li>
                        <li><a href="<?= site_url('#bater-papo'); ?>" title="Ir para Fale Conosco">fale conosco</a></li>
                        <li class="li-whatsapp">
                            <a class="a-whatsapp" target="_BLANK" onclick="ga('send','event', 'Whatsapp', 'click', 'Header');" href="https://api.whatsapp.com/send?phone=5511960761042" title="Ir para WhatsApp">
                                <span class="whats-icon"><i class="fab fa-whatsapp"></i></span>
                                <span class="whats-data">
                                    <span class="span-whatsapp-title">WhatsApp</span>
                                    <span class="span-whatsapp-number">11 96076-1042</span>
                                </span>
                            </a>
                        </li>
                        <li class="face">
                            <a href="https://www.facebook.com/JJ-V%C3%ADdeos-1584270475226113/?ref=hl" target="_blank"
                            class="face-icon"><i class="fab fa-facebook-f"></i></a>
                        </li>
                        <li class="tube">
                            <a href="https://www.youtube.com/channel/UCpDeVrBKHH2ofsDhp1NfPtg" target="_blank" class="tube-icon"><i
                                class="fab fa-youtube"></i></a>
                            </li>
                        </ul>
                    </div>

                </div>

            </div>
        </header>



        <body <?php body_class(); ?>>