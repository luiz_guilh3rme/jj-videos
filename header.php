<!DOCTYPE html>

<html lang="en">

    <head>

        <!-- Google Tag Manager -->

        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({'gtm.start':
                            new Date().getTime(), event: 'gtm.js'});
                var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);

            })(window, document, 'script', 'dataLayer', 'GTM-N82DFB7');</script>

        <!-- End Google Tag Manager -->


        <script>

    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {

            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)

    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');



</script>


        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, user-scalable=no">

        <meta name="apple-mobile-web-app-title" content="Homepage" />

        <meta name="logo150x150" content=".png" />

        <title>

            <?php echo PW_SITE_NAME; ?>

        </title>

        <meta content="<?php echo PW_SITE_DESCRIPTION; ?>" name="description">


        <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/bootstrap/css/bootstrap.min.css">


        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt"

              crossorigin="anonymous">


        <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
        <link type="text/css" rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/extras/style.css?id=<?php echo time(); ?>">


        <?php wp_head(); ?>






    </head>











    <header class="container-fluid header-nav">

        <div class="row">
            <div class="col-xs-24 navigation no-pad">

                <div class="col-xs-24 logo-mobile">

                    <a href="http://www.jjvideos.com.br/">

                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.png" alt="logo jjvideos">

                    </a>

                    <div id="bt-mobile" class="header-toggle">

                        <i class="icon-nav">

                            <span class="inner-icon-nav"></span>

                        </i>

                    </div>

                </div>

                <div class="col-xs-24 menu">

                    <a class="logo" href="http://www.jjvideos.com.br" title="Ir para Home"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.png"

                                                                                                alt="logo jjvideos"></a>










                    <ul class="menu-list">
                        <li class="sub-nav"><a title="Ir para Planos">serviços</a>
                            <div class="columns-wrapper">
                                <ul>
                                    <li><a href="<?= site_url('/videos-animados/'); ?>" title="Ir para Videos Animados">Vídeos Animados</a>  </li>
                                    <li><a href="<?= site_url('/videos-explicativos/'); ?>" title="Ir para Videos Explicativos">Vídeos Explicativos</a></li>
                                    <li><a href="<?= site_url('/videos-produtos-servicos/'); ?>" title="Ir para Vídeos Produtos/Serviços">Videos para Produtos / Serviços</a> </li>
                                    <li><a href="<?= site_url('/videos-marketing/'); ?>" title="Ir para Vídeos Marketing"> Vídeos Marketing</a></li>
                                    <li><a href="<?= site_url('/videos-para-web/'); ?>" title="Ir para Vídeos Web">Vídeos para Web</a></li>
                                </ul>

                                <ul>
                                    <li><a href="<?= site_url('/videos-institucionais/'); ?>" title="Ir para Vídeos Institucionais"> Vídeos Institucionais</a></li>
                                    <li><a href="<?= site_url('/videos-infograficos/'); ?>" title="Ir para Vídeos Infográficos">Vídeos Infográficos</a></li>
                                    <li><a href="<?= site_url('/videos-de-apresentacao/'); ?>" title="Ir para Vídeos Apresentação">Vídeos de Apresentação</a></li>
                                    <li><a href="<?= site_url('/videos-de-animacao-3d/'); ?>" title="Ir para Vídeos de Animação 3D">Vídeos de Animação 3D</a></li>
                                    <li><a href="<?= site_url('/videos-de-animacao-2d/'); ?>" title="Ir para Vídeos de animação 2D">Vídeos de Animação 2D</a></li>
                                </ul>

                            </div>

                        </li>

                        <li><a href="<?= site_url('#portfolio'); ?>" title="Ir para Portfólio">portfólio</a></li>

                        <li class="sub-nav"><a title="Ir para Planos">planos</a>

                            <div class="columns-wrapper">

                                <ul>

                                    <li>

                                        <a href="<?= site_url('/pacote-basico/'); ?>" title="Ir para Pacote Básico">

                                            Básico</a>

                                    </li>

                                    <li>

                                        <a href="<?= site_url('/pacote-profissional/'); ?>" title="Ir para Pacote Profissional">

                                            Profissional</a>

                                    </li>

                                    <li>

                                        <a href="<?= site_url('/pacote-premium/'); ?>" title="Ir para Pacote Premium">

                                            Premium</a>

                                    </li>

                                    <li>

                                        <a href="<?= site_url('/pacote-expert/'); ?>" title="Ir para Pacote Expert">

                                            Expert</a>

                                    </li>

                                </ul>

                            </div>

                        </li>

                        <li><a href="<?= site_url('#blog'); ?>" title="Ir para Blog">blog</a></li>

                        <li><a href="<?= site_url('#bater-papo'); ?>" title="Ir para Fale Conosco">fale conosco</a></li>

                        <li class="li-whatsapp">

                            <a class="a-whatsapp" target="_BLANK" rel="nofollow" href="wpp" title="Ir para WhatsApp">

                                <span class="whats-icon"><i class="fab fa-whatsapp"></i></span>

                                <span class="whats-data">

                                    <span class="span-whatsapp-title">WhatsApp</span>

                                    <span class="span-whatsapp-number">11 96076-1042</span>

                                </span>

                            </a>

                        </li>

                        <li class="face">

                            <a href="https://www.facebook.com/JJ-V%C3%ADdeos-1584270475226113/?ref=hl" target="_blank" rel="nofollow"

                               class="face-icon"><i class="fab fa-facebook-f"></i></a>

                        </li>

                        <li class="tube">

                            <a href="https://www.youtube.com/channel/UCpDeVrBKHH2ofsDhp1NfPtg" target="_blank" class="tube-icon" rel="nofollow"><i

                                    class="fab fa-youtube"></i></a>

                        </li>

                    </ul>

                </div>



            </div>



        </div>

    </header>







    <body <?php body_class(); ?> >

        <div class="whatsApp">
            <div>
                <a href="wpp" class="btnWhatsAppLateral" target="_BLANK" >
                    <img src="http://www.jjvideos.com.br/wp-content/uploads/2018/10/whatsapp.png" alt="jjvideos via whatsapp">
                </a>
            </div>
        </div>
        <div class="whatsAppFooter">
            <div>
                <div><img class="iconeWhats" src="<?php echo get_template_directory_uri("jjvideos") ?>/img/whatasppIcone.png"></div>
                <a target="_blank" href="wpp" class="btnWhatsAppFooter">
                    whatsApp
                </a>
            </div>
        </div>


        <!-- Google Tag Manager (noscript) -->

        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N82DFB7"

                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

        <!-- End Google Tag Manager (noscript) -->











