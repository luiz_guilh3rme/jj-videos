
<section id="portfolio" class="container-fluid" name="portfolio">
    <div class="row no-gutters">

        <div class="col-xs-24 inner-container no-pad img-area">


            <!----- Portifolio Video Começo ----->
            <?php if(get_field('trabalhos', 4)): $i = 0; ?>
            <?php while(has_sub_field('trabalhos', 4)): $i++; ?>
            <div class="col-xs-12 no-pad port-col">
                <a class="teste" type="button" data-toggle="modal" data-target="#modalport-<?php echo $i; ?>">
                    <img src="<?php the_sub_field('thumb'); ?>" alt="">
                    <div class="content">
                        <img alt="<?php echo get_template_directory_uri(); ?>/img/portfolio-0.jpg">
                        <div class="outer-content">
                            <div class="inner-content">
                                <h5>
                                    <?php the_sub_field('titulo', 4, false); ?>
                                </h5>
                                <div class="small-line"></div>
                                <img src="<?php echo get_template_directory_uri(); ?>/img/icon-play-red.png">
                                <p>
                                    <?php the_sub_field('desc', 4, false); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </a>
                <!-- Modal -->
                <div class="modal fade" id="modalport-<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="vertical-alignment-helper">
                        <div class="modal-dialog vertical-align-center" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">FECHAR &times;</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">
                                        <?php the_sub_field('titulo', 4, false); ?>
                                    </h4>
                                </div>
                                <div class="modal-body">


                                    <iframe id="frame-<?php echo $i; ?>" src="https://www.youtube.com/embed/<?php the_sub_field('video', 4); ?>"
                                        frameborder="0" allowfullscreen></iframe>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <?php endwhile; ?>
            <?php endif; ?>
            <!----- Portifolio Video Fim----->

        </div>

    </div>

    <div class="container">
        <div class="row btn-area">
            <div class="col-xs-24">
                <a class="btn-style btn-white actionport">veja mais</a>
            </div>
        </div>
    </div>
</section>