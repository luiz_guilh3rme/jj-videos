<?php get_header(); ?>
   <header class="container-fluid header-nav">
        <div class="row">

            <!--Navigation Area -->
            <div class="col-xs-24 navigation no-pad">

                <div class="col-xs-24 logo-mobile">
                    <a>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.png" alt="logo jjvideos">
                    </a>
                    <div id="bt-mobile" class="header-toggle">
                        <i class="icon-nav">
                                <span class="inner-icon-nav"></span>
                            </i>
                    </div>
                </div>

                <div class="col-xs-24 menu">
                    <a class="logo" href="#hero"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.png" alt="logo jjvideos"></a>
                    <ul class="menu-list">
                        <li><a href="#portfolio">portfólio</a></li>
<!--                         <li><a href="#budget-option">planos</a></li>
 -->                        <li><a href="#blog">blog</a></li>
                        <li><a href="#bater-papo">fale connosco</a></li>
                        <li>
                            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-facebook.png" alt="facebook" /></a>
                        </li>
                        <li>
                            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-youtube.png" alt="youtube" /></a>
                        </li>
                    </ul>
                </div>

            </div>
            <!--Navigation Area -->

        </div>
    </header>
    
<?php if (have_posts()) : ?>
    <section id="post-page" class="container-fluid" name="partners">
<figure class="img-blog"><?php echo the_post_thumbnail('post-thumbnails')?></figure>
        <div class="container">

                    <div class="row">
                        <div class="col-xs-24">
                            <h4 class="title"><?php the_title() ?></h4>
                           <h6>Posted on: <?php the_time('j, F, Y'); ?> at <?php the_time('g:i a'); ?></h6>
                            <div class="small-line"></div>
                        </div>
                    </div>
                    
                    
            <div class="row">

                <div class="col-xs-24 inner-container">
                   
                <?php while (have_posts()) : the_post(); ?>
                    <article class="team-member">
                        <?php the_content(); ?>
                    </article>
                <?php endwhile; ?>
                
                <div id="posts-link" class="clearfix">
                    <hr>
                    <?php
                    $prev = get_adjacent_post(false, '', true);
                    $next = get_adjacent_post(false, '', false);

                    //use an if to check if anything was returned and if it has, display a link
                    if ($prev) {
                        $url = get_permalink($prev->ID);
                        echo '<a class="prev" href="' . $url . '" title="' . $prev->post_title . '"><< ' . $prev->post_title . '</a>';
                    }

                    if ($next) {
                        $url = get_permalink($next->ID);
                        echo '<a class="next" href="' . $url . '" title="' . $next->post_title . '">' . $next->post_title . ' >></a>';
                    }
                    ?>
                </div>
                
                </div>

            </div>
        </div>
    </section>
<?php endif; ?>
<?php get_footer(); ?>

