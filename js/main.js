    // MAIN MENU ANIMATION

    // Cache selectors
    var lastId,
        topMenu = $(".ancor, .hero"),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function () {
            var item = $($(this).attr("href"));
            if (item.length) {
                return item;
            }
        });

    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function (e) {

        var href = $(this).attr("href")
            //offsetTop = href === "#" ? 0 : $(href).offset().top+2;  
        $('html, body').stop().animate({
            scrollTop: href === "#" ? 0 : $(href).offset().top - 100
        }, 300);

        e.preventDefault();
    });
/*
    // Bind to scroll
    $(window).scroll(function () {
        // Get container scroll position
        var fromTop = $(this).scrollTop();

        // Get id of current scroll item
        var cur = scrollItems.map(function () {
            var test = $(this).offset().top;

            test = $(this).offset().top - 122;

            if (test < fromTop)
                return this;
        });
        // Get the id of the current element
        cur = cur[cur.length - 1];
        var id = cur && cur.length ? cur[0].id : "";

        if (lastId !== id) {
            lastId = id;
            // Set/remove active class
            menuItems
                .parent().removeClass("active-Menu")
                .end().filter("[href=#" + id + "]").parent().addClass("active-Menu");
        }
    });
*/



    // Animation - Click bt-mobile
    $("#bt-mobile").click(function (ev) {
        $(".menu").slideToggle("200", function () {
            $(".inner-icon-nav").parent().toggleClass('nav-in');
            //$(".menu ul.menu-list li").toggleClass('sub-desktop');
        });
        ev.stopPropagation();
    });


    /* Classes change event */
    $('.actionport').click(function () {
        $('.port-col').toggleClass('active-port');
    });
    
    $('.sub-nav').click(function () {
        $(this).toggleClass('active-sub');
    });



 





    /*
    $(document).ready(function() {
    var movementStrength = 25;
    var height = movementStrength / $(window).height();
    var width = movementStrength / $(window).width();
        $(".waves-bg1").mousemove(function(e){
              var pageX = e.pageX - ($(window).width() / 2);
              var pageY = e.pageY - ($(window).height() / 2);
              var newvalueX = width * pageX * -1 - 25;
              var newvalueY = height * pageY * -1 - 50;
              $('.waves-bg1').css("background-position", newvalueX+"px     "+newvalueY+"px");
        });
    });
    */
