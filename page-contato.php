<?php get_header(); ?>
   <header class="container-fluid header-nav">
        <div class="row">

            <!--Navigation Area -->
            <div class="col-xs-24 navigation no-pad">

                <div class="col-xs-24 logo-mobile">
                    <a>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.png" alt="logo jjvideos">
                    </a>
                    <div id="bt-mobile" class="header-toggle">
                        <i class="icon-nav">
                                <span class="inner-icon-nav"></span>
                            </i>
                    </div>
                </div>

                <div class="col-xs-24 menu">
                    <a class="logo" href="<?php echo get_site_url(); ?>/home/#hero"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.png" alt="logo jjvideos"></a>
                    <ul class="menu-list">
                        <li class="ancor"><a href="<?php echo get_site_url(); ?>/home/#portfolio">portfólio</a></li>
                        <li class="ancor"><a href="<?php echo get_site_url(); ?>/home/#budget-option">planos</a></li>
                        <li class="ancor"><a href="<?php echo get_site_url(); ?>/home/#blog">blog</a></li>
                        <li class="ancor"><a href="<?php echo get_site_url(); ?>/home/#bater-papo">fale connosco</a></li>
                        <li>
                            <a href="https://www.facebook.com/JJ-V%C3%ADdeos-1584270475226113/?ref=hl" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-facebook.png" alt="facebook" /></a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/channel/UCpDeVrBKHH2ofsDhp1NfPtg" rel="nofollow" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-youtube.png" alt="youtube" /></a>
                        </li>
                    </ul>
                </div>

            </div>
            <!--Navigation Area -->

        </div>
    </header>
    

    <section id="post-page" class="container-fluid" name="partners">
<figure class="img-blog"></figure>
        <div class="container">

                    <div class="row">
                        <div class="col-xs-24">
                            <h4 class="title"></h4>
                            <h6>Posted on</h6>
                            <div class="small-line"></div>
                        </div>
                    </div>
                    
                    
            <div class="row">

                <div class="col-xs-24 inner-container">
                   
                    <article class="team-member">
                        <?php echo do_shortcode( '[contact-form-7 id="292" title="Sem nome"]' );?>
                    </article>
                
                <div id="posts-link" class="clearfix">
                    <hr>
                   
                </div>
                
                </div>

            </div>
        </div>
    </section>
<?php get_footer(); ?>

