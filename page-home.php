<?php get_header(); ?>


<!-- <header class="container-fluid header-nav">
        <div class="row">

            <div class="col-xs-24 navigation no-pad">

                <div class="col-xs-24 logo-mobile">
                    <a>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.png" alt="logo jjvideos">
                    </a>
                    <div id="bt-mobile" class="header-toggle">
                        <i class="icon-nav">
                                <span class="inner-icon-nav"></span>
                            </i>
                    </div>
                </div>

                <div class="col-xs-24 menu">
                    <a class="logo" href="#hero"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.png" alt="logo jjvideos"></a>
                    <ul class="menu-list">
                        <li class="ancor"><a href="#portfolio">portfólio</a></li>
                        <li class="ancor"><a href="#budget-option">planos</a></li>
                        <li class="ancor"><a href="#blog">blog</a></li>
                        <li class="ancor"><a href="#bater-papo">fale conosco</a></li>
                        <li>
                            <a href="https://www.facebook.com/JJ-V%C3%ADdeos-1584270475226113/?ref=hl" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-facebook.png" alt="facebook" /></a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/channel/UCpDeVrBKHH2ofsDhp1NfPtg" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-youtube.png" alt="youtube" /></a>
                        </li>
                    </ul>
                </div>


            </div>

        </div>
    </header> -->


<section id="hero" class="container-fluid hero" name="hero">
    <div class="container">
        <div class="row">
            <div class="video-bg">
                <?php if(get_field('video_background')): ?>
                <?php while(has_sub_field('video_background')): ?>
                <video autoplay poster="<?php the_sub_field('capa'); ?>" id="bgvid" loop>
                    <!-- WCAG general accessibility recommendation is that media such as background video play through only once. Loop turned on for the purposes of illustration; if removed, the end of the video will fade in the same way created by pressing the "Pause" button  -->
                    <source src="<?php the_sub_field('video_webm'); ?>" type="video/webm">
                    <source src="<?php the_sub_field('video_mp4'); ?>" type="video/mp4">
                </video>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>

            <div class="col-xs-24 content">
                <h1>
                    <span class="line1">
                        <?php the_field('primeira_linha', false, true); ?></span>
                    <span class="line2">
                        <?php the_field('segunda_linha', false, true); ?></span>
                    <span class="line3">
                        <?php the_field('terceira_linha', false, true); ?></span>
                    <span class="line4">
                        <?php the_field('quarta_linha', false, true); ?></span>
                </h1>
                <!--<a class="btn-style btn-white">veja o nosso demo reel</a>-->


                <!-- Button trigger modal -->
                <button type="button" class="btn-style btn-white" data-toggle="modal" data-target="#myModalreel">
                    veja o nosso demo reel
                </button>

                <!-- Modal -->
                <div class="modal fade" id="myModalreel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="vertical-alignment-helper">
                        <div class="modal-dialog vertical-align-center" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">FECHAR &times;</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">demo reel</h4>
                                </div>
                                <div class="modal-body">


                                    <iframe id="frame-reel" src="https://www.youtube.com/embed/<?php the_field('demo_reel', false, false); ?>"
                                        frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <a href="#our-videos"><i></i></a>


        </div>
    </div>
</section>


<section id="our-videos" class="container-fluid" name="our-videos">
    <div class="container">
        <div class="row">

            <div class="col-xs-24 inner-container">
                <div class="row">
                    <div class="title-outer">
                        <ul>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                        <h4 class="title">vídeos com superpoderes</h4>
                    </div>
                    <div class="small-line"></div>
                </div>


                <div class="row">
                    <div class="col-xs-24 no-gutters">
                        <div class="row content">
                            <!-- OUR VIDEO 1 -->
                            <div class="col-xs-24 col-xs-8 super-pod">
                                <?php if(get_field('icones')): ?>
                                <?php while(has_sub_field('icones')): ?>
                                <img src="<?php the_sub_field('icone_1'); ?>">
                                <?php endwhile; ?>
                                <?php endif; ?>

                                <?php if(get_field('titulos_super_poderes')): ?>
                                <?php while(has_sub_field('titulos_super_poderes')): ?>
                                <h5>
                                    <?php the_sub_field('titulo_1', false, false); ?>
                                </h5>
                                <?php endwhile; ?>
                                <?php endif; ?>

                                <span>
                                    <?php if(get_field('textos_super_poderes')): ?>
                                    <?php while(has_sub_field('textos_super_poderes')): ?>
                                    <p>
                                        <?php the_sub_field('primeiro_texto', false, false); ?>
                                    </p>
                                    <?php endwhile; ?>
                                    <?php endif; ?>
                                </span>
                            </div>
                            <!-- OUR VIDEO 2 -->
                            <div class="col-xs-24 col-xs-8 super-pod">
                                <?php if(get_field('icones')): ?>
                                <?php while(has_sub_field('icones')): ?>
                                <img src="<?php the_sub_field('icone_2'); ?>">
                                <?php endwhile; ?>
                                <?php endif; ?>

                                <?php if(get_field('titulos_super_poderes')): ?>
                                <?php while(has_sub_field('titulos_super_poderes')): ?>
                                <h5>
                                    <?php the_sub_field('titulo_2', false, false); ?>
                                </h5>
                                <?php endwhile; ?>
                                <?php endif; ?>

                                <span>
                                    <?php if(get_field('textos_super_poderes')): ?>
                                    <?php while(has_sub_field('textos_super_poderes')): ?>
                                    <p>
                                        <?php the_sub_field('segundo_texto', false, false); ?>
                                    </p>
                                    <?php endwhile; ?>
                                    <?php endif; ?>
                                </span>
                            </div>
                            <!-- OUR VIDEO 3 -->
                            <div class="col-xs-24 col-xs-8 super-pod">
                                <?php if(get_field('icones')): ?>
                                <?php while(has_sub_field('icones')): ?>
                                <img src="<?php the_sub_field('icone_3'); ?>">
                                <?php endwhile; ?>
                                <?php endif; ?>

                                <?php if(get_field('titulos_super_poderes')): ?>
                                <?php while(has_sub_field('titulos_super_poderes')): ?>
                                <h5>
                                    <?php the_sub_field('titulo_3', false, false); ?>
                                </h5>
                                <?php endwhile; ?>
                                <?php endif; ?>

                                <span>
                                    <?php if(get_field('textos_super_poderes')): ?>
                                    <?php while(has_sub_field('textos_super_poderes')): ?>
                                    <p>
                                        <?php the_sub_field('terceiro_texto', false, false); ?>
                                    </p>
                                    <?php endwhile; ?>
                                    <?php endif; ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<?php get_template_part('includes/portfolio'); ?>


<div class="container-fluid row-line blue">
    <div class="full-line blue-inner"></div>
</div>


<section id="video-percent" class="container-fluid" name="video-percent">
    <div class="container">
        <div class="row">

            <div class="col-xs-24 inner-container no-gutters">
                <div class="row content">
                    <div class="col-xs-24 col-sm-6">
                        <?php if(get_field('titulos')): ?>
                        <?php while(has_sub_field('titulos')): ?>
                        <h5>
                            <?php the_sub_field('titulo_1', false, false); ?>
                        </h5>
                        <?php endwhile; ?>
                        <?php endif; ?>
                        <div class="small-line"></div>
                        <?php if(get_field('porcentagens')): ?>
                        <?php while(has_sub_field('porcentagens')): ?>
                        <h2>
                            <?php the_sub_field('porcentagens_1', false, false); ?>
                        </h2>
                        <?php endwhile; ?>
                        <?php endif; ?>
                        <?php if(get_field('Texto')): ?>
                        <?php while(has_sub_field('Texto')): ?>
                        <h4>
                            <?php the_sub_field('texto_1', false, false); ?>
                        </h4>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-xs-24 col-sm-6">
                        <?php if(get_field('titulos')): ?>
                        <?php while(has_sub_field('titulos')): ?>
                        <h5>
                            <?php the_sub_field('titulo_2', false, false); ?>
                        </h5>
                        <?php endwhile; ?>
                        <?php endif; ?>
                        <div class="small-line"></div>
                        <?php if(get_field('porcentagens')): ?>
                        <?php while(has_sub_field('porcentagens')): ?>
                        <h2>
                            <?php the_sub_field('porcentagens_2', false, false); ?>
                        </h2>
                        <?php endwhile; ?>
                        <?php endif; ?>
                        <?php if(get_field('Texto')): ?>
                        <?php while(has_sub_field('Texto')): ?>
                        <h4>
                            <?php the_sub_field('texto_2', false, false); ?>
                        </h4>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-xs-24 col-sm-6">
                        <?php if(get_field('titulos')): ?>
                        <?php while(has_sub_field('titulos')): ?>
                        <h5>
                            <?php the_sub_field('titulo_3', false, false); ?>
                        </h5>
                        <?php endwhile; ?>
                        <?php endif; ?>
                        <div class="small-line"></div>
                        <?php if(get_field('porcentagens')): ?>
                        <?php while(has_sub_field('porcentagens')): ?>
                        <h2>
                            <?php the_sub_field('porcentagens_3', false, false); ?>
                        </h2>
                        <?php endwhile; ?>
                        <?php endif; ?>
                        <?php if(get_field('Texto')): ?>
                        <?php while(has_sub_field('Texto')): ?>
                        <h4>
                            <?php the_sub_field('texto_3', false, false); ?>
                        </h4>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-xs-24 col-sm-6">
                        <?php if(get_field('titulos')): ?>
                        <?php while(has_sub_field('titulos')): ?>
                        <h5>
                            <?php the_sub_field('titulo_4', false, false); ?>
                        </h5>
                        <?php endwhile; ?>
                        <?php endif; ?>
                        <div class="small-line"></div>
                        <?php if(get_field('porcentagens')): ?>
                        <?php while(has_sub_field('porcentagens')): ?>
                        <h2>
                            <?php the_sub_field('porcentagens_4', false, false); ?>
                        </h2>
                        <?php endwhile; ?>
                        <?php endif; ?>
                        <?php if(get_field('Texto')): ?>
                        <?php while(has_sub_field('Texto')): ?>
                        <h4>
                            <?php the_sub_field('Texto_4', false, false); ?>
                        </h4>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="row content-bottom">
                    <div class="col-xs-24">
                        <h5>
                            <?php the_field('frase', false, true); ?>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid row-line blue">
    <div class="full-line blue-inner"></div>
</div>

<section id="budget-option" class="container-fluid" name="budget-option">

    <div class="waves-bg2"></div>
    <div class="waves-bg1"></div>

    <div class="container">
        <!---- Planos ---->

        <div class="row">
            <div class="col-xs-24 inner-container">
                <div class="row">
                    <h4 class="title">planos</h4>
                    <div class="small-line"></div>
                </div>
                <div class="row content">

                    <div class="planos-edit">
                        <a class="option option1" type="button" href="http://www.jjvideos.com.br/pacote-basico/">
                            <div class="gradient-bg"></div>
                            <div class="outer-option"></div>
                            <div class="inner-option">
                                <h4>Básico</h4>
                                <p>Transmita sua ideia de forma objetiva e cativante para conquistar seus clientes</p>
                            </div>
                        </a>
                    </div>
                    <div class="planos-edit">
                        <a class="option option1" type="button" href="http://www.jjvideos.com.br/pacote-profissional/">
                            <div class="gradient-bg"></div>
                            <div class="outer-option"></div>
                            <div class="inner-option">
                                <h4>Profissional</h4>
                                <p>Mais qualidade gráfica e melhores opções criativas vão comunicar melhor todos os
                                    benefícios de seu negócio/ideia além de ajudar a diferenciar sua marca</p>
                            </div>
                        </a>
                    </div>
                    <div class="planos-edit">
                        <a class="option option1" type="button" href="http://www.jjvideos.com.br/pacote-premium/">
                            <div class="gradient-bg"></div>
                            <div class="outer-option"></div>
                            <div class="inner-option">
                                <h4>Premium</h4>
                                <p>Estilo de animação 3D totalmente customizado para atingir seu público-alvo</p>
                            </div>
                        </a>
                    </div>
                    <div class="planos-edit">
                        <a class="option option1" type="button" href="http://www.jjvideos.com.br/pacote-expert/">
                            <div class="gradient-bg"></div>
                            <div class="outer-option"></div>
                            <div class="inner-option">
                                <h4>Expert</h4>
                                <p>Ilustração/Desenhos feitos sob medida. Cenários e Personagens desenhados do zero,
                                    feitos exclusivamente para o seu projeto.</p>
                            </div>
                        </a>
                    </div>





                    <!-- <div class="row">
                <div class="col-xs-24 inner-container">
                    <div class="row">
                        <h4 class="title">plano</h4>
                        <div class="small-line"></div>
                    </div>
                    <div class="row content">
                       
                            <?php if(get_field('planos')): $i = 0;  ?>
                                <?php while(has_sub_field('planos')): $i++;  ?>
                        <div class="planos-edit">
                            <a class="option option1" type="button" data-toggle="modal" data-target="#modalplan-<?php echo $i; ?>">
                                <div class="gradient-bg"></div>
                                <div class="outer-option"></div>
                                <div class="inner-option">
                                    <h4><?php the_sub_field('titulo_plano', false, false); ?></h4>
                                    <p><?php the_sub_field('chamada_plano', false, false); ?></p>
                                </div>
                            </a>
                        </div>
                        

                        <div class="modal fade" id="modalplan-<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="vertical-alignment-helper">
                                <div class="modal-dialog vertical-align-center" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">FECHAR &times;</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">Vídeos explicativos</h4>
                                        </div>
                                        <div class="modal-body">

                                            <p><?php the_sub_field('desc_plano', false, false); ?></p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                                <?php endwhile; ?>
                            <?php endif; ?>

                    </div>
                </div>

            </div> -->

                </div>
            </div>

</section>


<section id="budget-option-btn" class="container-fluid ancor">
    <a href="#bater-papo" class="btn-style btn-black">solicitar orçamento</a>
</section>


<section id="blog" class="container-fluid" name="blog">
    <div class="row no-margin">
        <div class="col-xs-24">
            <h4 class="title">blog</h4>
            <div class="small-line"></div>
        </div>
    </div>
    <div class="row no-margin content">

        <!--<div class="col-xs-24 col-md-6 no-pad blog blog1">

                <div class="img-blog"><img src="<?php echo get_template_directory_uri(); ?>/img/blog-1.jpg" alt="img blog1"></div>
                <div class="inner-blog">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/blog-0-empty.jpg" alt="img blog vazio">
                    <div class="content-blog">
                        <h6>4 Tipos de Vídeos para alavancar seu negócio</h6>
                        <div class="small-line"></div>
                        <span>
                                <p>1 – Contando uma história (storytelling) Este tipo de vídeo é utilizado para contar uma história relacionada à sua empresa. Muito utilizada em anúncios, a técnica de storytelling busca transmitir ideias e valores que... </p>
                            </span>
                    </div>
                </div>
                <div class="img-blog"><img src="<?php echo get_template_directory_uri(); ?>/img/blog-1.jpg" alt="img blog1"></div>

            </div>-->

        <?php // WP_Query arguments
			$args = array (
				'post_type'              => array( 'post' ),
				'posts_per_page'         => '4',
                'paged'                  => $paged,
			);

			// The Query
			$query = new WP_Query( $args );
			// The Loop
			if ( $query->have_posts() ) {
				while ( $query->have_posts() ) {
					$query->the_post();?>

        <div class="col-xs-24 col-md-6 no-pad blog">
            <a href="<?php the_permalink(); ?>">
                <figure class="img-blog"><img class="arrow-blog" src="<?php echo get_template_directory_uri(); ?>/img/arrow-blog.png">
                    <?php echo the_post_thumbnail('home-thumbnails')?>
                </figure>
                <div class="inner-blog">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/blog-0-empty.jpg" alt="img blog vazio">
                    <div class="content-blog">
                        <h6>
                            <?php the_title(); ?>
                        </h6>
                        <div class="small-line"></div>
                        <span>
                            <?php the_excerpt('new_excerpt_length') ?>
                        </span>
                        <div class="clock-home">
                            <p><img src="<?php echo get_template_directory_uri(); ?>/img/clock-home-icon.jpg">
                                <?php the_time('j F, Y'); ?>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="img-blog"><img class="arrow-blog" src="<?php echo get_template_directory_uri(); ?>/img/arrow-blog.png">
                    <?php echo the_post_thumbnail('home-thumbnails')?>
                </div>
            </a>
        </div>


        <?php }
			}
			
			// Restore original Post Data
			wp_reset_postdata();
			
			?>



</section>


<section id="bater-papo" class="container-fluid" name="partners">
    <div class="container">
        <div class="row">

            <div class="col-xs-24 inner-container">
                <div class="row">
                    <div class="col-xs-24 social-net">
                        <div class="social-net-inner">

                            <?php if(get_field('rede_sociais')): ?>
                            <?php while(has_sub_field('rede_sociais')): ?>
                            <a href="<?php the_sub_field('url'); ?>" target="_blank"><img src="<?php the_sub_field('icone'); ?>"></a>
                            <?php endwhile; ?>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-24">
                        <h4 class="title">
                            <?php the_field('titulo_contato', false, true); ?>
                        </h4>
                        <div class="small-line"></div>
                    </div>
                </div>

                <div class="row form-area">
                    <div class="col-xs-24 contact">
                        <ul>
                            <li>
                                <a target="_BLANK" class="whatsapp-internas" href="wpp" rel="nofollow" title="Ir para WhatsApp">
                                    <i class="fab fa-whatsapp"></i>
                                    <?php the_field('telefone', false, true); ?>
                                </a>
                            </li>
                            <li>
                                <a> <i class="fab fa-skype"></i> JJVIDEOSBRASIL</a>
                            </li>
                            <li>
                                <a href="mailto:<?php the_field('email', false, true); ?>"
                                    class="email-internas"
                                 target="_blank" title="Enviar E-mail">
                                    <i class="far fa-envelope"></i>
                                    <?php the_field('email', false, true); ?>
                                </a>
                            </li>
                        </ul>
                    </div>


                    <?php echo do_shortcode('[contact-form-7 id="250" title="contato 2"]'); ?>


                </div>

            </div>

        </div>
    </div>
</section>
<?php get_footer(); ?>