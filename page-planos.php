<?php 

/*
Template Name: Planos
*/

get_header(); ?>

<!--linear-gradient(to right, #0de1ea, #c6d92c)-->
<section class="container-fluid video_destaque" id="video_destaque" name="videos">
	<div class="container">
		<div class="row">
			<div class="col-xs-24">
				<h3 class="title"><div><?php the_title() ?></div></h3>
				<div class="small-line"></div>
			</div>
		</div>
	</div>  
	
	<div class="container">
		<div class="row">
			<div class="col-xs-14">
			<div class="video-container">
				<?php

				// get iframe HTML
				$iframe = get_field('video_destaque');


				// use preg_match to find iframe src
				preg_match('/src="(.+?)"/', $iframe, $matches);
				$src = $matches[1];


				// add extra params to iframe src
				$params = array(
					'controls'    => 0,
					'hd'        => 1,
					'autohide'    => 1
				);

				$new_src = add_query_arg($params, $src);

				$iframe = str_replace($src, $new_src, $iframe);


				// add extra attributes to iframe html
				$attributes = 'frameborder="0"';

				$iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);


				// echo $iframe
				echo $iframe;

				?>
			</div>
			<img class="videos-logo" src="http://www.jjvideos.com.br/wp-content/themes/jjvideos/img/logo-white.png" alt="logo jjvideos">
			</div>
		</div>
   </div>  
</section>

<?php if (have_posts()) : ?>
    <section id="post-page" style="margin-top:40px" class="container-fluid" name="partners">
             
        <div class="container">           
            <div class="row">
                <div class="col-xs-24 inner-container">
                    <?php while (have_posts()) : the_post(); ?>
                        <article class="team-member">
                            <?php the_content(); ?>
                        </article>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>  
    </section>
<?php endif; ?>
<?php 
get_template_part('includes/portfolio');
?>

	<section class="container-fluid  page-contact" name="partners">   
        <div class="container">
			<div class="row row-title">
				<h2 class="font_2">Contato/Orçamento</h2>
				<div class="small-line"></div>				
			</div>
		</div>

		<div class="container">  
			<div class="row">
				<div class="data-contact col-xs-8 div-data">
					<p>Quer saber mais?<br>
					Precisa ter uma ideia do investimento?<br>
					Entre em contato com a gente.</p>
					<ul>
						<li> 
							<a href="wpp" class="whatsapp-internas" rel="nofollow" title="Ir para WhatsApp" target="_blank"> 
								<i class="fab fa-whatsapp"></i>  11 96076-1042 </a>
						</li>
						<li> 
							<a> <i class="fab fa-skype"></i> JJVIDEOSBRASIL </a>
						</li>
						<li>
							<a href="mailto:CONATO@JJVIDEOS.COM.BR" target="_blank" title="Enviar E-mail" class="email-internas">
							<i class="far fa-envelope"></i>  CONTATO@JJVIDEOS.COM.BR </a>
						</li>
					</ul>
				</div>
				
				<div class="form-contact col-xs-16 div-form">
					<?php echo do_shortcode('[contact-form-7 id="250" title="contato 2"]'); ?>       
				</div>
			</div>
        </div>  
    </section>

<?php get_footer(); ?>

