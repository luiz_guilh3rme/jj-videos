<?php 
/*

Template Name: WhatsApp Redirect

*/

get_header(); ?>
   <header class="container-fluid header-nav">
        <div class="row">

            <!--Navigation Area -->
            <div class="col-xs-24 navigation no-pad">

                <div class="col-xs-24 logo-mobile">
                    <a href="http://www.jjvideos.com.br/">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.png" alt="logo jjvideos">
                    </a>
                    <div id="bt-mobile" class="header-toggle">
                        <i class="icon-nav">
                                <span class="inner-icon-nav"></span>
                            </i>
                    </div>
                </div>

                <div class="col-xs-24 menu">
                    <a class="logo" href="#hero"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.png" alt="logo jjvideos"></a>
                    <ul class="menu-list">
                        <li><a href="#portfolio">portfólio</a></li>
                        <li><a href="#budget-option">planos</a></li>
                        <li><a href="#blog">blog</a></li>
                        <li><a href="#bater-papo">fale conosco</a></li>
                        <li>
                            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-facebook.png" alt="facebook" /></a>
                        </li>
                        <li>
                            <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-youtube.png" alt="youtube" /></a>
                        </li>
                    </ul>
                </div>

            </div>
            <!--Navigation Area -->

        </div>
    </header>
    
<?php if (have_posts()) : ?>
    <section id="post-page" class="container-fluid" name="partners">
<figure class="img-blog"><?php echo the_post_thumbnail('post-thumbnails')?></figure>
        <div class="container">

                    <div class="row">
                        <div class="col-xs-24">
                            <h4 class="title">Redirecionando...</h4>
                            <div class="small-line"></div>
                        </div>
                    </div>
        </div>
    </section>


<script>
	setTimeout(function(){
	 window.location.href = 'https://api.whatsapp.com/send?phone=5511960761042';
         }, 2000);
</script>
<?php endif; ?>
<?php get_footer(); ?>

