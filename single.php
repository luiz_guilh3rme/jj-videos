<?php get_header(); ?>
  <!-- <header class="container-fluid header-nav">
        <div class="row">

            <div class="col-xs-24 navigation no-pad">

                <div class="col-xs-24 logo-mobile">
                    <a>
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.png" alt="logo jjvideos">
                    </a>
                    <div id="bt-mobile" class="header-toggle">
                        <i class="icon-nav">
                                <span class="inner-icon-nav"></span>
                            </i>
                    </div>
                </div>

                <div class="col-xs-24 menu">
                    <a class="logo" href="<?php echo get_site_url(); ?>/home/#hero"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-white.png" alt="logo jjvideos"></a>
                    <ul class="menu-list">
                        <li class="ancor"><a href="<?php echo get_site_url(); ?>/home/#portfolio">portfólio</a></li>
                        <li class="ancor"><a href="<?php echo get_site_url(); ?>/home/#budget-option">planos</a></li>
                        <li class="ancor"><a href="<?php echo get_site_url(); ?>/home/#blog">blog</a></li>
                        <li class="ancor"><a href="<?php echo get_site_url(); ?>/home/#bater-papo">fale connosco</a></li>
                        <li>
                            <a href="https://www.facebook.com/JJ-V%C3%ADdeos-1584270475226113/?ref=hl" rel="nofollow"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-facebook.png" alt="facebook" /></a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/channel/UCpDeVrBKHH2ofsDhp1NfPtg" rel="nofollow"><img src="<?php echo get_template_directory_uri(); ?>/img/icon-youtube.png" alt="youtube" /></a>
                        </li>
                    </ul>
                </div>

            </div>

        </div>
    </header>-->
    
<?php if (have_posts()) : ?>
    <section id="post-page" class="container-fluid" name="partners">
<figure class="img-blog"><?php echo the_post_thumbnail('post-thumbnails')?></figure>
        <div class="container">

                    <div class="row">
                        <div class="col-xs-24">
                            <h4 class="title"><?php the_title() ?></h4>
                            <h6>Posted on: <?php the_time('j, F, Y'); ?> at <?php the_time('g:i a'); ?></h6>
                        <div class="small-line"></div>
                        </div>
                    </div>
                    
                    
            <div class="row">

                <div class="col-xs-24 inner-container">
                   
                <?php while (have_posts()) : the_post(); ?>
                    <article class="team-member">
                        <?php the_content(); ?>
                    </article>
                <?php endwhile; ?>
                
                <div id="posts-link" class="clearfix">
                    <hr>
                    <?php
                    $prev = get_adjacent_post(false, '', true);
                    $next = get_adjacent_post(false, '', false);

                    //use an if to check if anything was returned and if it has, display a link
                    if ($next) {
                        $url = get_permalink($next->ID);
                        echo '<a class="next" href="' . $url . '" title="' . $next->post_title . '"><< ' . $next->post_title . '</a>';
                    }

                    if ($prev) {
                        $url = get_permalink($prev->ID);
                        echo '<a class="prev" href="' . $url . '" title="' . $prev->post_title . '"> ' . $prev->post_title . ' >></a>';
                    }
                    ?>
                </div>
                
                </div>

            </div>
        </div>
    </section>
<?php endif; ?>
<?php get_footer(); ?>

